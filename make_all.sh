#!/bin/bash

echo "====================="
echo "Compiling root folder"
echo "====================="
cd src
bash convert_all.sh
echo

echo "====================="
echo "Compiling blog folder"
echo "====================="
cd bloge
bash make_blogs.sh
echo

echo "========================="
echo "Compiling tutorial folder"
echo "========================="
cd ../tutorial
bash make_tutorials.sh
echo