#!/bin/bash

read -p 'Please provide Neocities username... ' user;
echo "Provide computer password if prompted...";
gpg pw.gpg;
echo

pass=$(cat pw);

# https://stackoverflow.com/questions/3685970/check-if-a-bash-array-contains-a-value
containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

#fetch html listing as array
curl "https://$user:$pass@neocities.org/api/list" > lsneo
listing=($(cat lsneo | jq '.files[] | {path: .path}' | sed -n "/path/p" | sed -e "s/\"\| //g" -e "s/path://g" | sed -n "/html$/p"))
#updates=($(cat lsneo | jq '.files[] | {updated_at: .updated_at}' | sed -n '/updated_at/p' | sed -e 's/\"\| //g' -e "s/updated_at://g"))

# https://gist.github.com/rchrd2/4fb3198053d107a81430e35a85a9e363
upload_file (){
    fullName=$1;
    shortName=${fullName:2};
    echo "$shortName check.";
    containsElement "$shortName" "${listing[@]}"
    if [ $? -ne 0 ]; then 
# upload only files not present on server
#    containsElement "$(LANG="en_US" TZ='UTC' date --date="$(stat $shortName | sed -n "/Modify/p" | awk '{$1="";print $0}' | cut -b2-)" +%a,%d%b%Y%H:%M:%S%z)" "${updates[@]}"
#    if [ $? -ne 0 ]; then 
    echo "uploading $shortName";
    curl -F "$shortName=@$fullName" "https://$user:$pass@neocities.org/api/upload";
#    fi;
    fi
}

cd html

# I hope this isn't too much for Neocities
find . -iname "*.html" | \
     while read file; do upload_file "$file"; done;

cd ..

rm lsneo

shred pw && rm pw;