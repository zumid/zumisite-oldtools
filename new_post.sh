#!/bin/bash

read -p 'Enter name of blog post...' title;
echo
echo "Create temporary file"
echo "DO NOT 'SAVE AS', THIS TMP. FILE WILL BE"
echo "RENAMED ON QUIT"
echo
touch /tmp/blogex;
leafpad /tmp/blogex 2>/dev/null &&
echo -e "<!--title Blog: $title-->\n\n# $title\n#### `date +%Y.%m.%d\ %H:%M`\n\n`cat /tmp/blogex`" > src/bloge/`date +%Y%m%d`-`echo $title | tr [A-Z] [a-z] | sed -e "s/:\|\ /_/g"`.md
rm /tmp/blogex
echo "====================="
echo "Updating blog folder"
echo "====================="
cd src/bloge
bash make_blogs.sh
