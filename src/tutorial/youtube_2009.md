<!--title Make 2009 YouTube Videos-->

# How to make 2009 YouTube Videos

You'll need a couple of things:

* 009 Sound System - Dreamscape
* Notepad
* Windows XP (physical or virtual)
* Unregistered Hypercam 2 (a.k.a the version from 2007, it's free these days. Use Wayback Machine)
* Windows Movie Maker
* An idea