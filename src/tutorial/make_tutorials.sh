#!/bin/bash

echo "Force update tutorial folder"
rm ../../html/tutorial/*.html

echo "<!--title Tutorials-->" > index.md
echo  >> index.md
echo "# Tutorial index" >> index.md
echo >> index.md
echo '"You can do anything" - 009 Sound System' >> index.md
echo >> index.md

GLOBIGNORE="index.md"
    # hardcoded tutorials
    echo "* [Coloring tutorial](page/COLORING_Tutorial/coloring_tutorial_wip.html)"  >> index.md;
for i in *.md; do
    echo "... Making entry for `basename $i .md`";
    echo "* [`basename $i .md`](tutorial/`basename $i .md`.html)"  >> index.md;
done
GLOBIGNORE=""

for i in *.md; do
    echo "... Compiling $i";
    bash ../convert.sh $i;
done

for i in *.html;do
    echo "... Move $i to tutorial folder";
    mv $i ../../html/tutorial;
done

echo "Tutorial compile done."
