#!/bin/bash

echo "Force update blog folder"
rm ../../html/bloge/*.html

echo "<!--title Blog index-->" > index.md
echo  >> index.md
echo "# Blog index" >> index.md
echo >> index.md
echo "This is where I'll keep some blags, or something." >> index.md
echo >> index.md

for i in [0-9]*.md; do
    echo "... Making entry for `basename $i .md`";
    echo "* [`basename $i .md`](bloge/`basename $i .md`.html)"  >> index.md;
done

for i in *.md; do
    echo "... Compiling $i";
    bash ../convert.sh $i;
done

for i in *.html;do
    echo "... Move $i to blog folder";
    mv $i ../../html/bloge;
done

echo "Blog compile done."
