<!--title Blog: Posting from Shell-->

# Posting from Shell
#### 2018.10.21 08:37 

Well, this should be fun. How about making a blog post through a shell script? Well, it's not entirely shell I gotta give it that but, it's like WYSIWYG editors, except it's definitely not WYSIWYG. Instead it's Markdown, if you've ever used Reddit or Discord you should be familiar with it.

The way this works is by making a temporary file which will then have the title and date automatically added to it. Should I automatically invoke the update_blogs script from here? It could be good I think.

Stay tuned for the scripts, they might be posted to my GitHub
