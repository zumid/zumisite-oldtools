#!/bin/bash

echo "Force update root folder"
rm ../html/*.html 2>/dev/null

for i in *.md; do
    echo "... Compiling $i";
    bash convert.sh $i;
done

for i in *.html;do
    echo "... Move $i to root folder";
    mv $i ../html;
done

echo "Root compile done."