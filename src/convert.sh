#!/bin/bash

doconvert (){
echo "... ... inserting HTML header tags"
    echo                                                                                        > "$1.html";
    echo "<!DOCTYPE html>"                                                                      >> "$1.html";
    echo "<html>"                                                                               >> "$1.html";
    echo "<head>"                                                                               >> "$1.html";
echo "... ... inserting metadata and other parameters"
    cat "../hdr/head.snip"                                                                     >> "$1.html";
# Page title determined by <!--title XXX -->
echo "... ... set page title"
    echo "<title>"                                                                              >> "$1.html";
    sed -r -n -e "/<\!--title/p" < "$1.md" | sed "s/^<\!--title \|-->$//g"                      >> "$1.html";
    echo "- Zumi's Site"                                                                        >> "$1.html";
    echo "</title>"                                                                             >> "$1.html";
    echo "</head>"                                                                              >> "$1.html";
    echo "<body>"                                                                               >> "$1.html";
# Nitty grittt
    cat "../hdr/header.snip"                                                                   >> "$1.html";
echo "... ... converting contents from markdown to html"
    cat "$1.md" | pandoc -f markdown -t html | sed 's/^<!--.\+//g'                              >> "$1.html";
# End
    cat "../hdr/footer.snip"                                                                   >> "$1.html";
    echo "</body>"                                                                              >> "$1.html";
    echo "</html>"                                                                              >> "$1.html";
echo "... ... html file written"
}

if [ -z "$1" ]; then
    echo "Must provide one filename!";
else
    if [ $(echo $1 | rev | cut -b1-3) != "dm." ]; then
        echo "Not a markdown file"; exit;
    fi
    doconvert "$(basename "$1" .md)";
fi
