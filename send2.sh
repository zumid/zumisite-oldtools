#!/bin/bash

read -p 'Please provide Neocities username... ' user;
echo "Provide computer password if prompted...";
gpg pw.gpg;
echo

pass=$(cat pw);

# https://gist.github.com/rchrd2/4fb3198053d107a81430e35a85a9e363
upload_file (){
    fullName=$1;
    shortName=${fullName:2};
    echo "uploading $shortName";
    curl -F "$shortName=@$fullName" "https://$user:$pass@neocities.org/api/upload";
}

cd html

# I hope this isn't too much for Neocities
find . | \
     while read file; do upload_file "$file"; done;

cd ..

shred pw && rm pw;